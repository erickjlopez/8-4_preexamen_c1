function ajax(){

	const url =  "https://jsonplaceholder.typicode.com/todos";

	axios
	.get(url)
	.then((res)=>{
		mostrar(res.data);
	})
	.catch((err)=>{
		console.log("Surgio un error: " + err)
	})


	function mostrar(data){
		const res = document.getElementById("respuesta");
		res.innerHTML = "";

		for(let item of data){
			res.innerHTML += "<div class='alert alert-warning'>" +
							"<p><strong>USER ID: </strong>" + item.userId + "</p> <p><strong>ID: </strong>" + item.id  + "  <p><strong>TITULO: </strong>" + item.title + "</p> <p><strong> ESTADO: </strong>" + item.completed +
							"</p><div>";
		}

		

	}


}

const btnCargar = document.getElementById("btnCargar");
btnCargar.addEventListener("click", function(){
	ajax();
});

const btnLimpiar = document.getElementById("btnLimpiar");
btnLimpiar.addEventListener("click", function(){
	const r = document.getElementById("respuesta");
	r.innerHTML = "";
});