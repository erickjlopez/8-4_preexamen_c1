function ajax(){

	const url =  "https://jsonplaceholder.typicode.com/users";

	axios
	.get(url)
	.then((res)=>{
		mostrar(res.data);
	})
	.catch((err)=>{
		console.log("Surgio un error: " + err)
	})


	function mostrar(data){
		const res = document.getElementById("respuesta");
		
		let datoSel = document.getElementById('inputID').value;
		if(datoSel == ''){
			alert("No hay ID a buscar");
			return;
		}
		
		for(let item of data){
			if(datoSel == item.id){
				let NOMBRE = document.getElementById('nombre');	
				NOMBRE.innerHTML = NOMBRE.setAttribute("value", item.name);
				let USUARIO = document.getElementById('usuario');	
				USUARIO.innerHTML = USUARIO.setAttribute("value", item.username);
				let EMAIL = document.getElementById('email');	
				EMAIL.innerHTML = EMAIL.setAttribute("value", item.email);
				let CALLE = document.getElementById('calle');	
				CALLE.innerHTML = CALLE.setAttribute("value", item.address.street);
				let NUMERO = document.getElementById('numero');	
				NUMERO.innerHTML = NUMERO.setAttribute("value", item.address.suite);
				let CIUDAD = document.getElementById('ciudad');	
				CIUDAD.innerHTML = CIUDAD.setAttribute("value", item.address.city);
			}

		}

		

	}


}

const btnBuscar = document.getElementById("btnBuscar");
btnBuscar.addEventListener("click", function(){
	ajax();
});

const btnLimpiar = document.getElementById("btnLimpiar");
btnLimpiar.addEventListener("click", function(){
	const r = document.getElementById("respuesta");
	let NOMBRE = document.getElementById('nombre');	
	NOMBRE.innerHTML = NOMBRE.setAttribute("value", "");
	let USUARIO = document.getElementById('usuario');	
	USUARIO.innerHTML = USUARIO.setAttribute("value", "");
	let EMAIL = document.getElementById('email');	
	EMAIL.innerHTML = EMAIL.setAttribute("value", "");
	let CALLE = document.getElementById('calle');	
	CALLE.innerHTML = CALLE.setAttribute("value", "");
	let NUMERO = document.getElementById('numero');	
	NUMERO.innerHTML = NUMERO.setAttribute("value", "");
	let CIUDAD = document.getElementById('ciudad');	
	CIUDAD.innerHTML = CIUDAD.setAttribute("value", "");
});